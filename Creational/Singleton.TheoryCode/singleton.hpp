#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <memory>
#include <mutex>
#include <atomic>

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

    static Singleton& instance()
	{
        if (!instance_)
        {
            std::lock_guard<std::mutex> lk{mtx_};

            if (!instance_.load())
            {
                void* raw_mem = ::operator new(sizeof(Singleton));  // 1 - alokacja pamieci
                new (raw_mem) Singleton();  // 2 - utworzenie obiektu - placement new

                instance_.store(static_cast<Singleton*>(raw_mem)); // 3 - inicjalizacja wskanika

                //instance_ = new Singleton();
            }
        }

        return *instance_;
	}

	void do_something();

private:
    static std::mutex mtx_;
    static std::atomic<Singleton*> instance_;


	Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
    }

    ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
    {
        std::cout << "Singleton has been destroyed!" << std::endl;
    }

};

std::mutex Singleton::mtx_;
std::atomic<Singleton*> Singleton::instance_{nullptr};

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

#endif /*SINGLETON_HPP_*/
