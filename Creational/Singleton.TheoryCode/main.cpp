#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    cout << "Main starts..." << endl;

	Singleton::instance().do_something();

	Singleton& singleObject = Singleton::instance();
	singleObject.do_something();

    cout << "Main ends..." << endl;
}
