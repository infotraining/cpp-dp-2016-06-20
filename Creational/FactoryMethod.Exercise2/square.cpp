#include "square.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_registered
        = Drawing::ShapeFactory::instance()
            .register_creator("Square", [] { return new Square();});
}
