#include <iostream>
#include <memory>
#include <cassert>
#include <typeinfo>
#include <algorithm>
#include <locale>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    Engine* clone() const
    {
        Engine* cloned_engine = do_clone();
        assert(typeid(*cloned_engine) == typeid(*this));

        return cloned_engine;
    }

    virtual ~Engine() = default;
protected:
    virtual Engine* do_clone() const = 0;
};

class Diesel : public Engine
{
public:
    virtual void start() override
    {
        cout << "Diesel starts\n";
    }

    virtual void stop() override
    {
        cout << "Diesel stops\n";
    }

    // Engine interface
protected:
    Engine* do_clone() const
    {
        return new Diesel(*this);
    }
};

class TDI : public Diesel
{
public:
    virtual void start() override
    {
        cout << "TDI starts\n";
    }

    virtual void stop() override
    {
        cout << "TDI stops\n";
    }

    // Engine interface
protected:
    Engine* do_clone() const
    {
        return new TDI(*this);
    }
};

class Hybrid : public Engine
{
public:
    virtual void start() override
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop() override
    {
        cout << "Hybrid stops\n";
    }

    // Engine interface
protected:
    Engine* do_clone() const
    {
        return new Hybrid(*this);
    }
};

class Car
{
    Engine* engine_;
public:
    Car(Engine* engine) : engine_{ engine }
    {}

    Car(const Car& source) : engine_{source.engine_->clone()}
    {
    }

    ~Car()
    {
        delete engine_;
    }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1(new TDI());

    c1.drive(100);

    cout << "\n";

    Car c2 = c1;
    c2.drive(200);
}

