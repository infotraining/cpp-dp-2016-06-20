#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <map>
#include <algorithm>
#include <stdexcept>

#include "shape.hpp"
#include "shape_factory.hpp"
#include "application.hpp"
#include <memory>

using namespace Drawing;
using namespace std;

//class ShapeCreator
//{
//public:
//    virtual Shape* create_shape() = 0;
//    virtual ~ShapeCreator() = default;
//};

namespace Details
{

template <typename ProductType, typename IdType = std::string>
class GenFactory
{
    std::map<std::string, std::function<ProductType* ()>> creators_;
public:
    bool register_creator(const IdType& id, std::function<ProductType* ()> creator)
    {
        return creators_.insert(std::make_pair(id, std::move(creator))).second;
    }

    ProductType* create(const IdType& id) const
    {
        return creators_.at(id)();
    }
};

}

//Shape* create_shape(const std::string& id)
//{
//	if (id == "Circle")
//		return new Circle();
//	else if (id == "Rectangle")
//		return new Rectangle();

//	throw std::runtime_error("Bad identifier");
//}

class GraphicsDocument : public Document
{
	vector<Shape*> shapes_;
	string title_;
protected:
    virtual ShapeFactoryType& get_shape_factory()
    {
        return ShapeFactory::instance();
    }

public:
    GraphicsDocument()
    {
    }

	virtual void open(const string& file_name)
	{
		title_ = file_name;

		ifstream fin(file_name.c_str());

		string type_identifier;

		cout << "Loading a file...\n";
		while(!fin.eof())
		{
			if (fin >> type_identifier)
			{
				cout << type_identifier << "\n";

                Shape* shp_ptr = get_shape_factory().create_object(type_identifier);
				shp_ptr->read(fin);

				shapes_.push_back(shp_ptr);
			}
		}

		cout << "Loading finished." << endl;
	}

	virtual void save(const std::string& file_name)
	{
		ofstream fout(file_name.c_str());

        for(const auto& shape : shapes_)
            shape->write(fout);

		fout.close();

		title_ = file_name;
	}

	virtual string get_title() const
	{
		return title_;
	}

	void show()
	{
		cout << "\n\nDrawing:\n";

        for(const auto& shape : shapes_)
            shape->draw();
	}

	~GraphicsDocument()
	{
		for(std::vector<Shape*>::iterator it = shapes_.begin(); it != shapes_.end(); ++it)
			delete *it;
	}
};

class GraphicsApplication : public Application
{
public:
    GraphicsApplication()
    {}

	virtual Document* create_document()
	{
        return new GraphicsDocument();
	}
};

//class CircleCreator : public ShapeCreator
//{
//    // ShapeCreator interface
//public:
//    Shape*create_shape()
//    {
//        return new Circle();
//    }
//};

//class RectangleCreator : public ShapeCreator
//{

//    // ShapeCreator interface
//public:
//    Shape*create_shape()
//    {
//        return new Rectangle();
//    }
//};

//Shape* create_circle()
//{
//    return new Circle;
//}

int main()
{

    //shape_factory.register_creator("Circle", &create_circle);
    //shape_factory.register_creator("Rectangle", [] { return new Rectangle(); });

    GraphicsApplication app;

    app.open_document("../drawing.txt");

    Document* ptr_doc = app.find_document("../drawing.txt");

	ptr_doc->show();

	ptr_doc->save("new_drawing.txt");
}
