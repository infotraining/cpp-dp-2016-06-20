#include "circle.hpp"
#include "shape_factory.hpp"

namespace Drawing
{
    class CircleCreator
    {
    public:
        Shape* operator()()
        {
            return new Circle();
        }
    };
}


namespace
{
    bool is_registered = Drawing::ShapeFactory::instance()
                            .register_creator("Circle", Drawing::CircleCreator());
}
