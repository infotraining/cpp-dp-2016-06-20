## plik .profile:

```
export http_proxy=http://172.23.0.100:8080
export https_proxy=https://172.23.0.100:8080
```

## Books

* A Tour of C++ - Bjarne Stroustroup

## C++ Core Guidelines

* https://github.com/isocpp/CppCoreGuidelines

## Video

* https://www.youtube.com/watch?v=w1yPw0Wd6jA
* https://channel9.msdn.com/posts/C-and-Beyond-2011-Herb-Sutter-Why-C